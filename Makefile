# ************************************************************************** #
#                                                                            #
#                                                        :::      ::::::::   #
#   Makefile                                           :+:      :+:    :+:   #
#                                                    +:+ +:+         +:+     #
#   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        #
#                                                +#+#+#+#+#+   +#+           #
#   Created: 2016/02/29 23:55:49 by tjarross          #+#    #+#             #
#   Updated: 2016/02/29 23:55:49 by tjarross         ###   ########.fr       #
#                                                                            #
# ************************************************************************** #

LIB =		lib_vec.a
SRC_NAME =	normalize_vec.c dot_vec.c nmultiply_vec.c sub_vec.c add_vec.c \
			div_vec.c ndiv_vec.c not_vec.c multiply_vec.c length_vec.c \
			product_vec.c make_vec.c make_mat.c multiply_mat.c mult_vec_mat.c \
			vec_to_tab.c mat_to_tab.c identity_mat.c mvp_matrix.c add_mat.c
SRC =		$(addprefix src/, $(SRC_NAME))
OBJ =		$(SRC:.c=.o)
INC =		./include/lib_vec.h
CC =		gcc
FLAGS = 	-Wall -Werror -Wextra -I ./include

all: $(LIB)

msg:
	@echo "\033[0;29m⌛  Making Lib_vec : \c"

$(LIB): msg $(OBJ)
	@ar rcs $(LIB) $(OBJ)
	@ranlib $(LIB)
	@echo "\n\033[0;34m✅  Lib_vec Created !\033[0;29m"

%.o: %.c $(INC)
	@$(CC) $(FLAGS) -o $@ -c $<
	@echo "\033[0;32m.\c\033[0;29m"

clean:
	@rm -f $(OBJ)

fclean: clean
	@echo "\033[0;31m🔥  Cleaning Lib_vec Objects..."
	@echo "\033[0;31m🔥  Cleaning Lib_vec Library...\033[0;29m"
	@rm -f $(LIB)

re: fclean all

.PHONY: all clean fclean re msg
