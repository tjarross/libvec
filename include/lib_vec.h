/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_vec.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/29 17:05:06 by tjarross          #+#    #+#             */
/*   Updated: 2017/06/04 20:56:45 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIB_VEC_H
# define LIB_VEC_H

# include <math.h>
# include <float.h>

# ifndef LIBVEC_DOUBLE_PRECISION_ENABLED
#  define LVEC_FLOAT	float
# else
#  define LVEC_FLOAT	double
# endif

typedef LVEC_FLOAT	t_float;

typedef struct	s_vec2
{
	t_float	x;
	t_float	y;
}				t_vec2;

typedef struct	s_vec3
{
	t_float	x;
	t_float	y;
	t_float	z;
}				t_vec3;

typedef struct	s_vec4
{
	t_float	x;
	t_float	y;
	t_float	z;
	t_float	w;
}				t_vec4;

typedef struct	s_mat2
{
	t_float	a;
	t_float	b;
	t_float	c;
	t_float	d;
}				t_mat2;

typedef struct	s_mat3
{
	t_float	a;
	t_float	b;
	t_float	c;
	t_float	d;
	t_float	e;
	t_float	f;
	t_float	g;
	t_float	h;
	t_float	i;
}				t_mat3;

typedef struct	s_mat4
{
	t_float	a;
	t_float	b;
	t_float	c;
	t_float	d;
	t_float	e;
	t_float	f;
	t_float	g;
	t_float	h;
	t_float	i;
	t_float	j;
	t_float	k;
	t_float	l;
	t_float	m;
	t_float	n;
	t_float	o;
	t_float	p;
}				t_mat4;

t_vec2			add_vec2(t_vec2 v1, t_vec2 v2);
t_vec3			add_vec3(t_vec3 v1, t_vec3 v2);
t_vec4			add_vec4(t_vec4 v1, t_vec4 v2);

t_mat2			add_mat2(t_mat2 m1, t_mat2 m2);
t_mat3			add_mat3(t_mat3 m1, t_mat3 m2);
t_mat4			add_mat4(t_mat4 m1, t_mat4 m2);

t_vec2			ndiv_vec2(t_vec2 v1, t_float n);
t_vec3			ndiv_vec3(t_vec3 v1, t_float n);
t_vec4			ndiv_vec4(t_vec4 v1, t_float n);

t_float			dot_vec2(t_vec2 v1, t_vec2 v2);
t_float			dot_vec3(t_vec3 v1, t_vec3 v2);
t_float			dot_vec4(t_vec4 v1, t_vec4 v2);

t_float			length_vec2(t_vec2 v);
t_float			length_vec3(t_vec3 v);
t_float			length_vec4(t_vec4 v);

t_vec2			normalize_vec2(t_vec2 v);
t_vec3			normalize_vec3(t_vec3 v);
t_vec4			normalize_vec4(t_vec4 v);

t_vec2			nmultiply_vec2(t_vec2 v, t_float n);
t_vec3			nmultiply_vec3(t_vec3 v, t_float n);
t_vec4			nmultiply_vec4(t_vec4 v, t_float n);

t_vec2			ndiv_vec2(t_vec2 v, t_float n);
t_vec3			ndiv_vec3(t_vec3 v, t_float n);
t_vec4			ndiv_vec4(t_vec4 v, t_float n);

t_vec2			sub_vec2(t_vec2 v1, t_vec2 v2);
t_vec3			sub_vec3(t_vec3 v1, t_vec3 v2);
t_vec4			sub_vec4(t_vec4 v1, t_vec4 v2);

t_vec2			div_vec2(t_vec2 v1, t_vec2 v2);
t_vec3			div_vec3(t_vec3 v1, t_vec3 v2);
t_vec4			div_vec4(t_vec4 v1, t_vec4 v2);

t_vec2			not_vec2(t_vec2 v);
t_vec3			not_vec3(t_vec3 v);
t_vec4			not_vec4(t_vec4 v);

t_vec2			multiply_vec2(t_vec2 v1, t_vec2 v2);
t_vec3			multiply_vec3(t_vec3 v1, t_vec3 v2);
t_vec4			multiply_vec4(t_vec4 v1, t_vec4 v2);

t_vec3			product_vec3(t_vec3 v1, t_vec3 v2);

t_vec2			vec2(t_float x, t_float y);
t_vec3			vec3(t_float x, t_float y, t_float z);
t_vec4			vec4(t_float x, t_float y, t_float z, t_float w);

t_mat2			mat2(t_vec2 a, t_vec2 b);
t_mat3			mat3(t_vec3 a, t_vec3 b, t_vec3 c);
t_mat4			mat4(t_vec4 a, t_vec4 b, t_vec4 c, t_vec4 d);

t_mat2			multiply_mat2(t_mat2 m1, t_mat2 m2);
t_mat3			multiply_mat3(t_mat3 m1, t_mat3 m2);
t_mat4			multiply_mat4(t_mat4 m1, t_mat4 m2);

t_vec2			mult_vec_mat2(t_vec2 v, t_mat2 m);
t_vec3			mult_vec_mat3(t_vec3 v, t_mat3 m);
t_vec4			mult_vec_mat4(t_vec4 v, t_mat4 m);

t_float			*vec2_to_tab(t_vec2 v, t_float *tab);
t_float			*vec3_to_tab(t_vec3 v, t_float *tab);
t_float			*vec4_to_tab(t_vec4 v, t_float *tab);

t_float			*mat2_to_tab(t_mat2 m, t_float *tab);
t_float			*mat3_to_tab(t_mat3 m, t_float *tab);
t_float			*mat4_to_tab(t_mat4 m, t_float *tab);

t_mat2			identity_mat2(void);
t_mat3			identity_mat3(void);
t_mat4			identity_mat4(void);

t_mat4			projection_mat(t_float window_dim_ratio,
			t_float near_plane, t_float far_plane, t_float rad_angle);
t_mat4			view_mat(t_vec3 cam_pos, t_vec3 cam_dir, t_vec3 up);
t_mat4			transformation_mat(t_vec3 translate, t_vec3 rotate,
			t_float scale);
t_mat4			get_mvp(t_mat4 transfo_mat, t_mat4 view_mat, t_mat4 proj_mat);

#endif
