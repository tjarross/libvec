# Lib_vec #

### What is this repository for? ###

* Lib_vec is a C library and it's a personal project that provides useful functions for matrix and vectors operations.
* It can also generate a MVP matrix for graphics projects.

### How do I get set up? ###

* First clone the repository
```
#!sh
git clone https://tjarross@bitbucket.org/tjarross/lib-vec.git lib_vec
```
* Go to folder and compile the library
```
#!sh
cd lib_vec/
make
```
* Add the resulting binary lib_vec.a to your project by linking this lib with all your .o files
```
#!sh
gcc files.o lib_vec.a
```