/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   multiply_mat.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/04 20:30:19 by tjarross          #+#    #+#             */
/*   Updated: 2017/06/04 20:30:20 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_vec.h"

t_mat3	multiply_mat3(t_mat3 m1, t_mat3 m2)
{
	t_mat3 m;

	m.a = dot_vec3(vec3(m1.a, m1.b, m1.c), vec3(m2.a, m2.d, m2.g));
	m.b = dot_vec3(vec3(m1.a, m1.b, m1.c), vec3(m2.b, m2.e, m2.h));
	m.c = dot_vec3(vec3(m1.a, m1.b, m1.c), vec3(m2.c, m2.f, m2.i));
	m.d = dot_vec3(vec3(m1.d, m1.e, m1.f), vec3(m2.a, m2.d, m2.g));
	m.e = dot_vec3(vec3(m1.d, m1.e, m1.f), vec3(m2.b, m2.e, m2.h));
	m.f = dot_vec3(vec3(m1.d, m1.e, m1.f), vec3(m2.c, m2.f, m2.i));
	m.g = dot_vec3(vec3(m1.g, m1.h, m1.i), vec3(m2.a, m2.d, m2.g));
	m.h = dot_vec3(vec3(m1.g, m1.h, m1.i), vec3(m2.b, m2.e, m2.h));
	m.i = dot_vec3(vec3(m1.g, m1.h, m1.i), vec3(m2.c, m2.f, m2.i));
	return (m);
}

t_mat2	multiply_mat2(t_mat2 m1, t_mat2 m2)
{
	t_mat2 m;

	m.a = dot_vec2(vec2(m1.a, m1.b), vec2(m2.a, m2.c));
	m.b = dot_vec2(vec2(m1.a, m1.b), vec2(m2.b, m2.d));
	m.c = dot_vec2(vec2(m1.c, m1.d), vec2(m2.a, m2.c));
	m.d = dot_vec2(vec2(m1.c, m1.d), vec2(m2.b, m2.d));
	return (m);
}

t_mat4	multiply_mat4(t_mat4 m1, t_mat4 m2)
{
	t_mat4 m;

	m.a = dot_vec4(vec4(m1.a, m1.b, m1.c, m1.d), vec4(m2.a, m2.e, m2.i, m2.m));
	m.b = dot_vec4(vec4(m1.a, m1.b, m1.c, m1.d), vec4(m2.b, m2.f, m2.j, m2.n));
	m.c = dot_vec4(vec4(m1.a, m1.b, m1.c, m1.d), vec4(m2.c, m2.g, m2.k, m2.o));
	m.d = dot_vec4(vec4(m1.a, m1.b, m1.c, m1.d), vec4(m2.d, m2.h, m2.l, m2.p));
	m.e = dot_vec4(vec4(m1.e, m1.f, m1.g, m1.h), vec4(m2.a, m2.e, m2.i, m2.m));
	m.f = dot_vec4(vec4(m1.e, m1.f, m1.g, m1.h), vec4(m2.b, m2.f, m2.j, m2.n));
	m.g = dot_vec4(vec4(m1.e, m1.f, m1.g, m1.h), vec4(m2.c, m2.g, m2.k, m2.o));
	m.h = dot_vec4(vec4(m1.e, m1.f, m1.g, m1.h), vec4(m2.d, m2.h, m2.l, m2.p));
	m.i = dot_vec4(vec4(m1.i, m1.j, m1.k, m1.l), vec4(m2.a, m2.e, m2.i, m2.m));
	m.j = dot_vec4(vec4(m1.i, m1.j, m1.k, m1.l), vec4(m2.b, m2.f, m2.j, m2.n));
	m.k = dot_vec4(vec4(m1.i, m1.j, m1.k, m1.l), vec4(m2.c, m2.g, m2.k, m2.o));
	m.l = dot_vec4(vec4(m1.i, m1.j, m1.k, m1.l), vec4(m2.d, m2.h, m2.l, m2.p));
	m.m = dot_vec4(vec4(m1.m, m1.n, m1.o, m1.p), vec4(m2.a, m2.e, m2.i, m2.m));
	m.n = dot_vec4(vec4(m1.m, m1.n, m1.o, m1.p), vec4(m2.b, m2.f, m2.j, m2.n));
	m.o = dot_vec4(vec4(m1.m, m1.n, m1.o, m1.p), vec4(m2.c, m2.g, m2.k, m2.o));
	m.p = dot_vec4(vec4(m1.m, m1.n, m1.o, m1.p), vec4(m2.d, m2.h, m2.l, m2.p));
	return (m);
}
