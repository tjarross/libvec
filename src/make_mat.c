/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make_mat.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/04 20:37:13 by tjarross          #+#    #+#             */
/*   Updated: 2017/06/04 20:37:14 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_vec.h"

t_mat3	mat3(t_vec3 a, t_vec3 b, t_vec3 c)
{
	t_mat3	m;

	m.a = a.x;
	m.b = a.y;
	m.c = a.z;
	m.d = b.x;
	m.e = b.y;
	m.f = b.z;
	m.g = c.x;
	m.h = c.y;
	m.i = c.z;
	return (m);
}

t_mat2	mat2(t_vec2 a, t_vec2 b)
{
	t_mat2	m;

	m.a = a.x;
	m.b = a.y;
	m.c = b.x;
	m.d = b.y;
	return (m);
}

t_mat4	mat4(t_vec4 a, t_vec4 b, t_vec4 c, t_vec4 d)
{
	t_mat4	m;

	m.a = a.x;
	m.b = a.y;
	m.c = a.z;
	m.d = a.w;
	m.e = b.x;
	m.f = b.y;
	m.g = b.z;
	m.h = b.w;
	m.i = c.x;
	m.j = c.y;
	m.k = c.z;
	m.l = c.w;
	m.m = d.x;
	m.n = d.y;
	m.o = d.z;
	m.p = d.w;
	return (m);
}
