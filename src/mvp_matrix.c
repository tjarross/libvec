/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mvp_matrix.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/04 15:31:37 by tjarross          #+#    #+#             */
/*   Updated: 2017/06/04 20:32:39 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_vec.h"

t_mat4			projection_mat(t_float window_dim_ratio, t_float near_plane,
						t_float far_plane, t_float rad_angle)
{
	t_float y_scale;
	t_float x_scale;
	t_float frustrum_len;

	y_scale = (1 / tan(rad_angle)) * window_dim_ratio;
	x_scale = y_scale / window_dim_ratio;
	frustrum_len = far_plane - near_plane;
	return (mat4(vec4(x_scale, 0, 0, 0),
				vec4(0, y_scale, 0, 0),
				vec4(0, 0, -((far_plane + near_plane) / frustrum_len), -1),
				vec4(0, 0, -((2 * near_plane * far_plane) / frustrum_len), 0)));
}

t_mat4			view_mat(t_vec3 cam_pos, t_vec3 cam_dir, t_vec3 up)
{
	t_vec3 f;
	t_vec3 s;
	t_vec3 u;

	f = normalize_vec3(sub_vec3(cam_pos, cam_dir));
	s = normalize_vec3(product_vec3(f, up));
	u = normalize_vec3(product_vec3(s, f));
	return (mat4(vec4(s.x, u.x, -f.x, 0),
				vec4(s.y, u.y, -f.y, 0),
				vec4(s.z, u.z, -f.z, 0),
				vec4(-cam_pos.x, -cam_pos.y, -cam_pos.z, 1)));
}

static t_mat4	get_rotation_mat(t_vec3 rotate)
{
	t_mat4 rotx;
	t_mat4 roty;
	t_mat4 rotz;
	t_mat4 rot;

	rotx = mat4(vec4(1, 0, 0, 0),
				vec4(0, cos(rotate.x), sin(rotate.x), 0),
				vec4(0, -sin(rotate.x), cos(rotate.x), 0),
				vec4(0, 0, 0, 1));
	roty = mat4(vec4(cos(rotate.y), 0, -sin(rotate.y), 0),
				vec4(0, 1, 0, 0),
				vec4(sin(rotate.y), 0, cos(rotate.y), 0),
				vec4(0, 0, 0, 1));
	rotz = mat4(vec4(cos(rotate.z), sin(rotate.z), 0, 0),
				vec4(-sin(rotate.z), cos(rotate.z), 0, 0),
				vec4(0, 0, 1, 0),
				vec4(0, 0, 0, 1));
	rot = multiply_mat4(rotx, roty);
	rot = multiply_mat4(rot, rotz);
	return (rot);
}

t_mat4			transformation_mat(t_vec3 translate, t_vec3 rotate,
									t_float scale)
{
	t_mat4 translation_mat;
	t_mat4 rotation_mat;
	t_mat4 scale_mat;
	t_mat4 transformation_mat;

	rotation_mat = get_rotation_mat(rotate);
	translation_mat = mat4(vec4(1, 0, 0, 0),
							vec4(0, 1, 0, 0),
							vec4(0, 0, 1, 0),
							vec4(translate.x, translate.y, translate.z, 1));
	scale_mat = mat4(vec4(scale, 0, 0, 0),
					vec4(0, scale, 0, 0),
					vec4(0, 0, scale, 0),
					vec4(0, 0, 0, 1));
	transformation_mat = multiply_mat4(rotation_mat, translation_mat);
	transformation_mat = multiply_mat4(transformation_mat, scale_mat);
	return (transformation_mat);
}

t_mat4			get_mvp(t_mat4 transfo_mat, t_mat4 view_mat, t_mat4 proj_mat)
{
	return (multiply_mat4(multiply_mat4(transfo_mat, view_mat), proj_mat));
}
