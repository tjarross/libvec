/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   not_vec.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/29 17:58:01 by tjarross          #+#    #+#             */
/*   Updated: 2016/09/04 15:57:35 by pcrosnie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_vec.h"

t_vec3	not_vec3(t_vec3 v)
{
	v.x = -v.x;
	v.y = -v.y;
	v.z = -v.z;
	return (v);
}

t_vec2	not_vec2(t_vec2 v)
{
	v.x = -v.x;
	v.y = -v.y;
	return (v);
}

t_vec4	not_vec4(t_vec4 v)
{
	v.x = -v.x;
	v.y = -v.y;
	v.z = -v.z;
	v.w = -v.w;
	return (v);
}
