/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_dist.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/11 13:55:33 by tjarross          #+#    #+#             */
/*   Updated: 2017/06/04 20:45:26 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_vec.h"

t_float	length_vec3(t_vec3 v)
{
	return (sqrtf(dot_vec3(v, v)));
}

t_float	length_vec2(t_vec2 v)
{
	return (sqrtf(dot_vec2(v, v)));
}

t_float	length_vec4(t_vec4 v)
{
	return (sqrtf(dot_vec4(v, v)));
}
