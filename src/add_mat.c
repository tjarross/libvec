/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   add_mat.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/09 19:50:19 by tjarross          #+#    #+#             */
/*   Updated: 2017/06/04 20:41:20 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_vec.h"

t_mat3	add_mat3(t_mat3 m1, t_mat3 m2)
{
	m1.a += m2.a;
	m1.b += m2.b;
	m1.c += m2.c;
	m1.d += m2.d;
	m1.e += m2.e;
	m1.f += m2.f;
	m1.g += m2.g;
	m1.h += m2.h;
	m1.i += m2.i;
	return (m1);
}

t_mat2	add_mat2(t_mat2 m1, t_mat2 m2)
{
	m1.a += m2.a;
	m1.b += m2.b;
	m1.c += m2.c;
	m1.d += m2.d;
	return (m1);
}

t_mat4	add_mat4(t_mat4 m1, t_mat4 m2)
{
	m1.a += m2.a;
	m1.b += m2.b;
	m1.c += m2.c;
	m1.d += m2.d;
	m1.e += m2.e;
	m1.f += m2.f;
	m1.g += m2.g;
	m1.h += m2.h;
	m1.i += m2.i;
	m1.j += m2.j;
	m1.k += m2.k;
	m1.l += m2.l;
	m1.m += m2.m;
	m1.n += m2.n;
	m1.o += m2.o;
	m1.p += m2.p;
	return (m1);
}
