/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make_vec.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/04 20:42:01 by tjarross          #+#    #+#             */
/*   Updated: 2017/06/04 20:42:02 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_vec.h"

t_vec3	vec3(t_float x, t_float y, t_float z)
{
	return ((t_vec3){x, y, z});
}

t_vec2	vec2(t_float x, t_float y)
{
	return ((t_vec2){x, y});
}

t_vec4	vec4(t_float x, t_float y, t_float z, t_float w)
{
	return ((t_vec4){x, y, z, w});
}
