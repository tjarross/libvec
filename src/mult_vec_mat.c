/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mult_vec_mat.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/04 20:34:22 by tjarross          #+#    #+#             */
/*   Updated: 2017/06/04 20:36:01 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_vec.h"

t_vec3	mult_vec_mat3(t_vec3 v, t_mat3 m)
{
	t_vec3 res;

	res.x = v.x * m.a + v.y * m.b + v.z * m.c;
	res.y = v.x * m.d + v.y * m.e + v.z * m.f;
	res.z = v.x * m.g + v.y * m.h + v.z * m.i;
	return (res);
}

t_vec2	mult_vec_mat2(t_vec2 v, t_mat2 m)
{
	t_vec2 res;

	res.x = v.x * m.a + v.y * m.b;
	res.y = v.x * m.c + v.y * m.d;
	return (res);
}

t_vec4	mult_vec_mat4(t_vec4 v, t_mat4 m)
{
	t_vec4 res;

	res.x = v.x * m.a + v.y * m.b + v.z * m.c + v.w * m.d;
	res.y = v.x * m.e + v.y * m.f + v.z * m.g + v.w * m.h;
	res.z = v.x * m.i + v.y * m.j + v.z * m.k + v.w * m.l;
	res.w = v.x * m.m + v.y * m.n + v.z * m.o + v.w * m.p;
	return (res);
}
