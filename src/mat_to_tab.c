/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mat_to_tab.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/04 20:36:13 by tjarross          #+#    #+#             */
/*   Updated: 2017/06/04 20:36:19 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_vec.h"

t_float	*mat3_to_tab(t_mat3 m, t_float *tab)
{
	tab[0] = m.a;
	tab[1] = m.b;
	tab[2] = m.c;
	tab[3] = m.d;
	tab[4] = m.e;
	tab[5] = m.f;
	tab[6] = m.g;
	tab[7] = m.h;
	tab[8] = m.i;
	return (tab);
}

t_float	*mat4_to_tab(t_mat4 m, t_float *tab)
{
	tab[0] = m.a;
	tab[1] = m.b;
	tab[2] = m.c;
	tab[3] = m.d;
	tab[4] = m.e;
	tab[5] = m.f;
	tab[6] = m.g;
	tab[7] = m.h;
	tab[8] = m.i;
	tab[9] = m.j;
	tab[10] = m.k;
	tab[11] = m.l;
	tab[12] = m.m;
	tab[13] = m.n;
	tab[14] = m.o;
	tab[15] = m.p;
	return (tab);
}

t_float	*mat2_to_tab(t_mat2 m, t_float *tab)
{
	tab[0] = m.a;
	tab[1] = m.b;
	tab[2] = m.c;
	tab[3] = m.d;
	return (tab);
}
