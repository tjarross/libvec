/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   normalize.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/29 17:07:11 by tjarross          #+#    #+#             */
/*   Updated: 2016/10/09 13:59:35 by pcrosnie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_vec.h"

t_vec3	normalize_vec3(t_vec3 v)
{
	return (ndiv_vec3(v, sqrtf(dot_vec3(v, v))));
}

t_vec2	normalize_vec2(t_vec2 v)
{
	return (ndiv_vec2(v, sqrtf(dot_vec2(v, v))));
}

t_vec4	normalize_vec4(t_vec4 v)
{
	return (ndiv_vec4(v, sqrtf(dot_vec4(v, v))));
}
