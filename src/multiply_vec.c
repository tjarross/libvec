/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   product_vec.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/29 17:58:01 by tjarross          #+#    #+#             */
/*   Updated: 2017/06/04 20:37:02 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_vec.h"

t_vec3	multiply_vec3(t_vec3 v1, t_vec3 v2)
{
	v1.x *= v2.x;
	v1.y *= v2.y;
	v1.z *= v2.z;
	return (v1);
}

t_vec2	multiply_vec2(t_vec2 v1, t_vec2 v2)
{
	v1.x *= v2.x;
	v1.y *= v2.y;
	return (v1);
}

t_vec4	multiply_vec4(t_vec4 v1, t_vec4 v2)
{
	v1.x *= v2.x;
	v1.y *= v2.y;
	v1.z *= v2.z;
	v1.w *= v2.w;
	return (v1);
}
