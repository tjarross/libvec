/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dot_div.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/29 17:31:56 by tjarross          #+#    #+#             */
/*   Updated: 2016/09/08 15:27:21 by pcrosnie         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_vec.h"

t_vec3	div_vec3(t_vec3 v1, t_vec3 v2)
{
	v1.x /= v2.x;
	v1.y /= v2.y;
	v1.z /= v2.z;
	return (v1);
}

t_vec2	div_vec2(t_vec2 v1, t_vec2 v2)
{
	v1.x /= v2.x;
	v1.y /= v2.y;
	return (v1);
}

t_vec4	div_vec4(t_vec4 v1, t_vec4 v2)
{
	v1.x /= v2.x;
	v1.y /= v2.y;
	v1.z /= v2.z;
	v1.w /= v2.w;
	return (v1);
}
