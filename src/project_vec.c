/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   project_vec.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/04 20:33:49 by tjarross          #+#    #+#             */
/*   Updated: 2017/06/04 20:33:50 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_vec.h"

t_vec3	project_vec3(t_vec3 a, t_vec3 b)
{
	t_float t;

	t = dot_vec3(b, b);
	if (t)
		return (nmultiply_vec3(b, (dot_vec3(a, b) / t)));
	return (a);
}

t_vec2	project_vec2(t_vec2 a, t_vec2 b)
{
	t_float t;

	t = dot_vec2(b, b);
	if (t)
		return (nmultiply_vec2(b, (dot_vec2(a, b) / t)));
	return (a);
}

t_vec4	project_vec4(t_vec4 a, t_vec4 b)
{
	t_float t;

	t = dot_vec4(b, b);
	if (t)
		return (nmultiply_vec4(b, (dot_vec4(a, b) / t)));
	return (a);
}
