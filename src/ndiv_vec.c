/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   multiply_vec.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/29 17:15:58 by tjarross          #+#    #+#             */
/*   Updated: 2017/06/04 20:45:05 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_vec.h"

t_vec3	ndiv_vec3(t_vec3 v, t_float n)
{
	n = ((n > 0.f) ? (n) : (FLT_EPSILON));
	v.x /= n;
	v.y /= n;
	v.z /= n;
	return (v);
}

t_vec2	ndiv_vec2(t_vec2 v, t_float n)
{
	n = ((n > 0.f) ? (n) : (FLT_EPSILON));
	v.x /= n;
	v.y /= n;
	return (v);
}

t_vec4	ndiv_vec4(t_vec4 v, t_float n)
{
	n = ((n > 0.f) ? (n) : (FLT_EPSILON));
	v.x /= n;
	v.y /= n;
	v.z /= n;
	v.w /= n;
	return (v);
}
