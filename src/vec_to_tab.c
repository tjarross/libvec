/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec_to_tab.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/04 20:32:53 by tjarross          #+#    #+#             */
/*   Updated: 2017/06/04 20:33:01 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_vec.h"

t_float	*vec3_to_tab(t_vec3 v, t_float *tab)
{
	tab[0] = v.x;
	tab[1] = v.y;
	tab[2] = v.z;
	return (tab);
}

t_float	*vec4_to_tab(t_vec4 v, t_float *tab)
{
	tab[0] = v.x;
	tab[1] = v.y;
	tab[2] = v.z;
	tab[3] = v.w;
	return (tab);
}

t_float	*vec2_to_tab(t_vec2 v, t_float *tab)
{
	tab[0] = v.x;
	tab[1] = v.y;
	return (tab);
}
