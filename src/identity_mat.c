/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   identity_mat.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tjarross <tjarross@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/04 20:34:11 by tjarross          #+#    #+#             */
/*   Updated: 2017/06/04 20:36:48 by tjarross         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lib_vec.h"

t_mat3	identity_mat3(void)
{
	return (mat3(vec3(1, 0, 0),
				vec3(0, 1, 0),
				vec3(0, 0, 1)));
}

t_mat2	identity_mat2(void)
{
	return (mat2(vec2(1, 0),
				vec2(0, 1)));
}

t_mat4	identity_mat4(void)
{
	return (mat4(vec4(1, 0, 0, 0),
				vec4(0, 1, 0, 0),
				vec4(0, 0, 1, 0),
				vec4(0, 0, 0, 1)));
}
